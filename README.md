# Fluency CKEditor Plugin for ProcessWire
This plugin is designed to integrate the Fluency ProcessWire DeepL translation module with CKEditor. It's primary feature is providing the ability for the user to select text that will be ignored in translation.

## Work In Progress
This module is a work in progress and is not ready for use.
