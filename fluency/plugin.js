(function() {

	CKEDITOR.plugins.add('fluency', {
		onLoad: function() {
      CKEDITOR.addCss(
      	'span[data-fluency-ignore] {' +
				  'border-top: 1px dotted blue;' +
				  'border-bottom: 1px dotted blue;' +
				  'background-color: #B8EDFF;' +
				'}'
      );
    },
		// icons: 'justifyleft',
		init: function(editor) {
      var pluginDirectory = this.path;
        	editor.addContentsCss( pluginDirectory + 'styles/fluency.css' );

			/////////////
			// Methods //
			/////////////
			editor.addCommand('addTranslateIgnore', {
				exec : function( editor ) {
				  var selectedText = editor.getSelection().getSelectedText(),
				  		newElement = new CKEDITOR.dom.element("span");

				  newElement.setAttributes({'data-fluency-ignore': ''})
				  newElement.setText(selectedText);
				  editor.insertElement(newElement);
				}
			}) // addTranslateIgnore

			editor.addCommand('removeTranslateIgnore', {
				exec : function( editor ) {
				  var selectedText = editor.getSelection().getSelectedText(),
				  		element = editor.getStartElement()
				  // var newElement = new CKEDITOR.dom.element("span");

				  // newElement.setAttributes({'data-fluency-ignore': ''})
				  // newElement.setText(selectedText);
				  // editor.insertElement(newElement);
				}
			}) // removeTranslateIgnore

			//////////////
			// Triggers //
			//////////////
			editor.ui.addButton( 'FluencyIgnore', {
        label: 'Ignore During Translation',
        command: 'addTranslateIgnore',
        icon: this.path + 'images/translate_disable.png'
	    });

			editor.ui.addButton( 'FluencyUnignore', {
        label: 'Do Not Ignore During Translation',
        command: 'removeTranslateIgnore',
        icon: this.path + 'images/translate_enable.png'
	    });
		} // init
	}) // CKEDITOR.plugins.add
})();

